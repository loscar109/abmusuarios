@extends('layouts.app')

@section('content')
<div class="container">
<h1>No se permiten registros, si tiene cuenta, haga click en <a href="{{ route('login') }}">ingresar</a></h1>
</div>
@endsection
