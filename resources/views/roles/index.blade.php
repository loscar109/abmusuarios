@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                	<h2 class="mb-0">Gestión de Roles</h2>
                	<div class="btn-group">
	                	<a href="{{route('home')}}" class="btn btn-secondary">
						  Volver
						</a>
                		<!-- Button trigger modal -->
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#crear">
						  Crear
						</button>
					</div>
                </div>
                
                <div class="card-body">
                	@include ('layouts.mensaje')
                    <table class="table">
					  <thead>
					    <tr>
					      <th scope="col">Nombre</th>
					      <th scope="col">Opciones</th>
					    </tr>
					  </thead>
					  <tbody>
					  	@foreach($roles as $rol)
						    <tr>
							    <td>
		                            {{$rol->nombre}}
		                        </td>
		                        <td>
		                        	<!-- Button trigger modal -->
		                        	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editar{{$rol->id}}">
									  Editar
									</button>

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar{{$rol->id}}">
									  Eliminar
									</button>

									<!-- Modal -->
									<div class="modal fade" id="editar{{$rol->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									  <div class="modal-dialog" role="document">
									    <form class="card-body" method="post" action="{{route('roles.actualizar', $rol)}}">
					                        {{ csrf_field() }}
            								{{ method_field('put') }}


										    <div class="modal-content">
										      <div class="modal-header">
										        <h5 class="modal-title" id="exampleModalLabel">Editar Rol</h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										      <div class="modal-body">
										        <div class="form-group row">
					                            <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre</label>

					                            <div class="col-md-6">
					                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{old('nombre', $rol->nombre)}}" required autocomplete="nombre" autofocus>

					                                @error('nombre')
					                                    <span class="invalid-feedback" role="alert">
					                                        <strong>{{ $message }}</strong>
					                                    </span>
					                                @enderror
					                            </div>
					                        </div>
										      </div>
										      <div class="modal-footer">
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
										        <button type="submit" class="btn btn-success">Actualizar</button>
										      </div>
										    </div>

										</form>
									  </div>
									</div>

									

									<!-- Modal -->
									<div class="modal fade" id="eliminar{{$rol->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									  <div class="modal-dialog" role="document">

									  	<form action="{{route('roles.eliminar', $rol)}}" method="POST" class="modal-content">
		                                    @csrf
		                                    @method('DELETE')

										    <div class="modal-content">
										      <div class="modal-header">
										        <h5 class="modal-title" id="exampleModalLabel">Eliminar Rol</h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										      <div class="modal-body">
										        ¿Está seguro de eliminar el Rol {{$rol->nombre}}?
										      </div>
										      <div class="modal-footer">
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
										        <button type="submit" class="btn btn-danger">Eliminar</button>
										      </div>
										    </div>
										</form>
									  </div>
									</div>
		                        </td>
		                    </tr>
		                @endforeach
					  </tbody>
					</table>
                </div>


                <div class="modal fade" id="crear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				  	<form method="post" action="{{route('roles.guardar')}}" enctype="multipart/form-data">
                        @csrf

					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Crear Rol</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>

                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					        <button type="submit" class="btn btn-success">Guardar</button>
					      </div>
					    </div>

					</form>
				  </div>
				</div>
            </div>
        </div>
    </div>
</div>

@endsection

