@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                	<h2 class="mb-0">Gestión de Usuarios</h2>
                	<div class="btn-group">
	                	<a href="{{route('home')}}" class="btn btn-secondary">
						  Volver
						</a>
						<a href="{{route('usuarios.crear')}}" class="btn btn-primary">
						  Crear
						</a>
					</div>
                	
                </div>
                

                <div class="card-body">
                	@include ('layouts.mensaje')
                    <table class="table">
					  <thead>
					    <tr>
					      <th scope="col">Apellidos</th>
					      <th scope="col">Nombres</th>
					      <th scope="col">Rol</th>
					      <th scope="col">Opciones</th>
					    </tr>
					  </thead>
					  <tbody>
					  	@foreach($usuarios as $usuario)
						    <tr>
							    <td>
		                            {{$usuario->apellidos}}
		                        </td>
		                        <td>
		                            {{$usuario->name}}
		                        </td>
		                        <td>
		                            {{$usuario->rol->nombre}}
		                        </td>
		                        <td>
		                        	<!-- Button trigger modal -->
		                        	<button type="button" class="btn btn-info" data-toggle="modal" data-target="#ver{{$usuario->id}}">
									  Ver
									</button>

									<a class="btn btn-primary" href="{{ route('usuarios.editar', $usuario)}}">
			                            
			                                Editar
			                        </a>

									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar{{$usuario->id}}">
									  Eliminar
									</button>

									<div class="modal fade" id="ver{{$usuario->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									  <div class="modal-dialog" role="document">
									    <div class="modal-content">
									      <div class="modal-header">
									        <h5 class="modal-title" id="exampleModalLabel">Ver Usuario</h5>
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									          <span aria-hidden="true">&times;</span>
									        </button>
									      </div>
									      <div class="modal-body">
												<ul>
													<li>Apellidos: {{$usuario->apellidos}}</li>
													<li>Nombres: {{$usuario->name}}</li>
													<li>Correo Electrónico: {{$usuario->email}}</li>
													<li>Teléfono: {{$usuario->telefono}}</li>
													<li>Rol: {{$usuario->rol->nombre}}</li>
												</ul>
									         
									      </div>
									      <div class="modal-footer">
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
									        
									      </div>
									    </div>
									  </div>
									</div>

									<!-- Modal -->
									<div class="modal fade" id="eliminar{{$usuario->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									  <div class="modal-dialog" role="document">

									  	<form action="{{route('usuarios.eliminar', $usuario)}}" method="POST" class="modal-content">
		                                    @csrf
		                                    @method('DELETE')

										    <div class="modal-content">
										      <div class="modal-header">
										        <h5 class="modal-title" id="exampleModalLabel">Eliminar Usuario</h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										      <div class="modal-body">
										        ¿Está seguro de eliminar el Usuario {{$usuario->apellidos}} {{$usuario->name}}?
										      </div>
										      <div class="modal-footer">
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
										        <button type="submit" class="btn btn-danger">Eliminar</button>
										      </div>
										    </div>
										</form>
									  </div>
									</div>
		                        	
		                        </td>
		                    </tr>
		                @endforeach
					  </tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection