@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h3 class="mb-0">Principal</h3>
                </div>

                <div class="card-body">
                    @if (auth()->user()->rol_id==1)
                    
                    <div class="row">

                        <div class="col-12 col-lg-6">
                            <a href="{{ route('usuarios') }}" title="Gestión de Usuarios">
                                <img class="img-fluid mb-4" src="img/usuarios.svg" style="width: 480px;">
                            </a>
                        </div>

                        <div class="col-12 col-lg-6 py-3">
                            <a href="{{ route('roles') }}" title="Gestión de Roles">
                                <img class="img-fluid mb-4" src="img/permisos.svg" style="width: 480px; height: 200px;">
                            </a>
                        </div>


                    </div>
                    @elseif(auth()->user()->rol_id!=1)
                        Listado de Operaciones habilitadas para el Rol {{ Auth::user()->rol->nombre }}
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
