<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{

	//campos que se almacenan en la Base de Datos
    protected $fillable = [
        'nombre',
    ];

    // nombre de la tabla en la que se guardan los datos
    protected $table = 'roles';

    // función para obtener los Usuarios con un Rol especificado
    public function usuarios(){
        return $this->hasMany('App\User');
    } 

}
