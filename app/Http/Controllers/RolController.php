<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// usa el modelo Rol para las operaciones de ABM 
use App\Rol;
// usa el modelo User en el mètodo de eliminar
use App\User;

class RolController extends Controller
{
    public function index() { // muestra todos los roles registrados
    	// se obtienen los roles ordenados por nombre
    	$roles=Rol::orderBy('nombre', 'asc')->get();
    	// se retorna la vista, debe tener una ruta asociada, y el compact se le pasa a la vista para que se muestren los roles
    	return view('roles.index', compact('roles'));
    }

    public function guardar(Request $request) { // guarda un usuario con los datos ingresados en el formulario

    	//validaciones y mensajes
    	$request->validate([
            'nombre'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',
        ], [
            'required' => 'Campo requerido',
            'regex' => 'Complete el campo únicamente con letras',
            'nombre.max' => 'Se permiten hasta 50 caracteres',
        ]);


    	// cuando pasa las validaciones, se crea un nuevo rol
        Rol::create([
                    'nombre'=>$request->nombre,
                ]);

        //redirecciona a la ruta index de roles, y devuelve un mensaje
        return redirect()->route('roles')->withMessage('Rol guardado');
    }

    public function actualizar(Request $request, Rol $rol)
    {
    	// validaciones y mensajes
        $request->validate([
            'nombre'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',//regex con espacios
        ], [
            'required' => 'Campo requerido',
            'regex' => 'Complete el campo unicamente con letras',
            'nombre.max' => 'Se permiten hasta 50 caracteres',
        ]);

        // si pasa las validaciones, se actualiza el rol con los nuevos valores ingresados en el formulario por request
        $rol->update([
            'nombre' =>$request->nombre,
        ]);

        //retorna la vista con un mensaje
        return redirect()->route('roles')->withMessage('Rol actualizado');
    }

    public function eliminar(Rol $rol) { // elimina un rol seleccionado

    	//busca las referencias que tiene el rol con la tabla usuarios
        $references = User::whereRol_id($rol->id)->count();

        // si uno o mas usuarios tienen asignados el rol que se quiere eliminar, se devuelve un mensaje de error y finaliza la ejecuciòn
        if ($references > 0){ 
            return redirect()->route('roles')->withErrors('No se puede eliminar el Rol porque hay registros asociados');
        }
        
        // borra el rol y devuelve un mensaje
        $rol->delete();
        return redirect()->route('roles')->withMessage('Rol eliminado');
    }


}
