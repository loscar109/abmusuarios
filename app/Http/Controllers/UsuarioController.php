<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// usa el modelo User para las operaciones de ABM
use App\User;
// uso el modelo Rol para crear o editar un usuario
use App\Rol;
use Illuminate\Validation\Rule;

class UsuarioController extends Controller
{
    public function index() { // muestra todos los usuarios registrados
    	// se obtienen los usuarios ordenados por apellidos
    	$usuarios=User::orderBy('apellidos', 'asc')->get();
    	// se retorna la vista, debe tener una ruta asociada, y el compact se le pasa a la vista para que se muestren los usuarios
    	return view('usuarios.index', compact('usuarios'));
    }

    public function crear() { 
        //obtengo los roles y se cargan en la vista
        $roles=Rol::orderBy('nombre')->get();
        // se retorna la vista del formulario para crear un Usuario
        return view('usuarios.crear', compact('roles'));
    }

    public function guardar(Request $request) {
        //validaciones y mensajes
        $request->validate([
            'apellidos'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',
            'name'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:100',
            'email'=>'required|email|unique:users|max:100',
            'telefono'=>'required|regex:/[0-9]{10}/',
            'password'=>'required|max:50',
        ], [
            'required' => 'Campo requerido',
            'apellidos.regex' => 'Complete el campo únicamente con letras',
            'name.regex' => 'Complete el campo únicamente con letras',
            'unique' => 'La dirección de correo electrónico ya está en uso',
            'apellidos.max'=> 'Se permiten hasta 50 caracteres',
            'name.max'=> 'Se permiten hasta 100 caracteres',
            'email' => 'Correo Electrónico inválido',
            'password.max'=> 'Se permiten hasta 50 caracteres',
            'telefono.regex' => 'Complete el campo únicamente con números (10 dígitos)',
        ]);

        $request->merge(['password' => bcrypt($request->password)]);
        
        User::create($request->all());

        return redirect()->route('usuarios')->withMessage('Usuario creado');
    }

    public function editar(User $usuario) {
        //obtengo los roles y se cargan en la vista
        $roles=Rol::orderBy('nombre')->get();
        // se retorna la vista del formulario para editar un Usuario
        return view('usuarios.editar', compact('usuario','roles'));
    }

    public function actualizar(Request $request, User $usuario) {

        //validaciones y mensajes
        $request->validate([
            'apellidos'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',
            'name'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:100',
            'email'=>[ Rule::unique('users')->ignore($usuario),
                        'required', 'email', 'max:50' ],
            'telefono'=>'required|regex:/[0-9]{10}/',
            //'password'=>'required|max:50',
        ], [
            'required' => 'Campo requerido',
            'apellidos.regex' => 'Complete el campo únicamente con letras',
            'name.regex' => 'Complete el campo únicamente con letras',
            'unique' => 'La dirección de correo electrónico ya está en uso',
            'apellidos.max'=> 'Se permiten hasta 50 caracteres',
            'name.max'=> 'Se permiten hasta 100 caracteres',
            'email' => 'Correo Electrónico inválido',
            'telefono.regex' => 'Complete el campo únicamente con números (10 dígitos)',
        ]);
        
        // se actualiza el registro 
        $usuario->update([
            'apellidos' =>$request->apellidos,
            'name' =>$request->name,
            'email' =>$request->email,
            'rol_id'=>$request->rol_id,
            'telefono' =>$request->telefono,
        ]);

        return redirect()->route('usuarios')->withMessage('Usuario actualizado');
    }



    public function eliminar(User $usuario) { // elimina un usuario seleccionado  
        //en el caso que el usuario seleccionado sea el autenticado, se muestra un mensaje de error y se corta la ejecución
        if ($usuario->id===auth()->id()) {
            return redirect()->route('usuarios')->withErrors('El usuario no puede eliminarse a si mismo');
        }      
        // borra el usuario y devuelve un mensaje
        $usuario->delete();
        return redirect()->route('usuarios')->withMessage('Usuario eliminado');
    }
}
