<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function()
{


	Route::prefix('roles')->group(function() {
		Route::get('/', 'RolController@index')->name('roles');
		Route::post('crear', 'RolController@guardar')->name('roles.guardar');
		Route::put('editar/{rol}', 'RolController@actualizar')->name('roles.actualizar');
		Route::delete('eliminar/{rol}', 'RolController@eliminar')->name('roles.eliminar');
	});

	Route::prefix('usuarios')->group(function() {
		Route::get('/', 'UsuarioController@index')->name('usuarios');

		Route::get('crear', 'UsuarioController@crear')->name('usuarios.crear');
		Route::post('crear', 'UsuarioController@guardar')->name('usuarios.guardar');
		
		Route::get('editar/{usuario}', 'UsuarioController@editar')->name('usuarios.editar');
		Route::put('editar/{usuario}', 'UsuarioController@actualizar')->name('usuarios.actualizar');
		
		Route::delete('eliminar/{usuario}', 'UsuarioController@eliminar')->name('usuarios.eliminar');
	});

});	

