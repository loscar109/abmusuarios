<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

    	//ejecución de seeder por orden
        $this->call(RolSeeder::class);
    	$this->call(UserSeeder::class);

    }
}
