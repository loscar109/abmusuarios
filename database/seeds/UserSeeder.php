<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'apellidos'=>'Rodríguez',
	        'name'=>'Jorge Fernando',
	        'email'=> 'jorge@mail.com',
	        'password'=> bcrypt(12345678),
	        'telefono'=>'3758612546',
	        'rol_id'=>2,
        ]);

        DB::table('users')->insert([
        	'apellidos'=>'Villalba',
	        'name'=>'Carlos Lisandro',
	        'email'=> 'carlos@mail.com',
	        'password'=> bcrypt(12345678),
	        'telefono'=>'3764618446',
	        'rol_id'=>1,
        ]);

    }
}
